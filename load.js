//    alert( JSON.stringify(data) );

var loadI=0;
var LastAction = new Date().getTime();
var items_count=0;
var items_page=1;
var get_items_req = '';


$(document).ready(function(){
  browse_list_items();
  get_cars();
  get_user();
  setTimeout(function(){testReady()},900);
});

function get_user() {
  $('#usersjs').remove();
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = base + 'GetUser_JSONP.aspx';
  script.id = 'usersjs';
  $('head').append(script);
}

function jsonp_user(data) {
 if(data.person) {
   var content = new Array();
   content.push('<span>'+ data.person +'</span>');
   content.push('<a class="opener" accesskey="M">open</a>');
   content.push('<div class="slide">');
   content.push('<ul class="slide-list">');
   content.push('<li><a href="#popup9" class="modal-opener">Account Info</a></li>');
   //content.push('<li><a href="#popup9" class="modal-opener">Edit Profile</a></li>');
   content.push('<li><a href="'+base+'Login.aspx?logout=TRUE" class="modal-opener">Log Out</a></li>');
   content.push('<li><a href="http://support.webarchives.com" class="modal-opener">Help</a></li>');
   content.push('<li><span class="other">Version  8.0.1</span></li>');
   content.push('</ul>');
   content.push('</div>');
   $('.user-block').html(content.join(''));
   $('.userNameFirst').val(data.FirstName);
   $('.userNameLast').val(data.LastName);
   $('.userEmail').val(data.email);
   $('.userTitle').val(data.Title);
   $('.userImage').attr('src',data.Base + '&size=1&width=133&height=163');
 }
 else {
   $('#user-block').html('');
 }
 if(loadI<3) get_items();
 loadI++;
} 

function downloadCar(x) {
  $('#cdown').remove();
  $('#downloadTypes').html('');
  $('.downloadTypes >span:eq(0)').css('visibility','hidden');
  var iframe = document.createElement('iframe');
  iframe.src = base + 'DownloadOriginal.aspx?id=' + x.join(',');
  iframe.id = 'cdown';
  $('body').append(iframe);
}

function getitemcontactsheets() {
  if(selected.length>0) $('#contactSheet').attr('href',base + 'getContactSheet_JSONP.aspx?contactsheetitems=' + selected.join(',') + '&fimagepreview=true&columns='+ escape($('#contactPerPage').val()) +'&Heading='+ escape($('#contactHeading').val()) +'&ffiletype=true&ffilename=true&fassetdescription=true&ffilesize&fprojectname=true&fassetid&fdate=true&fdimensions=true' + '&' +x());
  else $('#contactSheet').removeAttr('href');
}

function getitemdownloads() {
  $('#idownjs').remove();
  $('#downloadTypes').html('');
  $('.downloadTypes >span:eq(0)').css('visibility','hidden');
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = base + 'getitemdownloads_jsonp.aspx?id=' + selected.join(',');
  script.id = 'idownjs';
  $('head').append(script);
}

function jsonp_group_downloads(data) {
   var content='';
   if(data.value.DownloadTypes.length>0){
    for (var i = 0; i < data.value.DownloadTypes.length; i++) { 
     content += '<option value="'+ data.value.DownloadTypes[i].URL +'">' + data.value.DownloadTypes[i].Type + '</option>';
    }
    $('#downloadTypes').html(content);
   }
   else $('#downloadTypes').html('<option value="">No Downloads Available</option>');

   jcf.customForms.destroyAllX('#downloadSelected');
   jcf.customForms.replaceAllX('#downloadSelected');
   $('.downloadTypes >span:eq(0)').css('visibility','visible');
}


function createCar(name,description,keywords,shared) {
  $('#createcarsjs').remove();
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = base + 'SaveCarousel_JSONP.aspx?Name='+escape(name)+'&description='+escape(description)+'&keywords='+escape(keywords)+'&isShared='+escape(shared)+'&password=';
  script.id = 'createcarsjs';
  $('head').append(script);
}

function jsonp_SaveCarousel(data) {
 setTimeout(function(){$('#createCar')[0].reset()},1000);
 get_cars();
}

function get_cars() {
  $('#cars').html('').attr('style','');
  $('#carsjs').remove();
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = base + 'GetCarousels_JSONP.aspx';
  script.id = 'carsjs';
  $('head').append(script);
}

function jsonp_carousel_list(data) {
   $('#cars').html('').attr('style','');
   $('.carsnumber').html('0');
   var x = data.rows[1].value.length;
   if(x>0) {
    var content = new Array();
    content.push('<ul class="carousel-list">');
    for (var i = 0; i < x; i++) { 
     content.push('<li><a rel="'+ data.rows[1].value[i].id + '"><em>'+ data.rows[1].value[i].Name + ((data.rows[1].value[i].isShared) ? '</em> <span>(shared)</span>' : '</em>') + '</a></li>');
    }
    content.push('</ul>');
    $('#cars').html(content.join(''));
   }
   $('.menu-slide a.delete').closest('li').remove();
   initCar();
} 

function delete_carousel(id) {
  $('#cardel').html('');
  $('.footer-holder .menu-opener, .footer-holder .menu-open, .number.carsnumber').hide();
  $('#carjs').remove();
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = base + 'DeleteCarousel_JSONP.aspx?Carousel_Id=' + id;
  script.id = 'cardel';
  $('head').append(script);
}

function jsonp_DeleteCarousel(data) {
    if(data.status=="true") get_cars();
    else alert(data.message );
}

function get_car(id) {
  $('.carouselTools').hide();
  $('#carname').html('');
  $('#carjs').remove();
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = base + 'GetCarouselItems_JSONP.aspx?carId=' + id;
  script.id = 'carjs';
  $('head').append(script);
}

function jsonp_GetCarouselItems(data) {
   $('#car').html('');
   var content = new Array();
   if(data.rows.length>0){
    content.push('<div class="carWrapper">');
    for (var i = 0; i < data.rows.length; i++) { 
     content.push('<div class="slide">');
     content.push('<img src="'+ data.rows[i].base + '&type=asset&id='+data.rows[i].id+'&size=2&width=144&height=106&qfactor=12" alt="" title="'+ data.rows[i].name +'">');
     content.push('<a href="#details" data-type="asset" rel="'+ data.rows[i].id +'" class="add information lightbox"></a>');
     content.push('<a rel="'+ data.rows[i].id +'" class="close remove"></a>');
     content.push('</div>');
    }
    content.push('<em class="C"></em></div>');
    $('#footer #car').html(content.join(''));
   }
   $('#p-name').val(data.name);
   $('#p-desc').val(data.description);
   if(data.isShared) $('#rad-public').prop('checked', true);
   else $('#rad-public').prop('checked', false);



   $('#detailsContent').html(content.join(''));
   $('.carsnumber').html(data.rows.length);
   $('#carname').html(data.name).attr('data-car',data.id);
   if(!$('.menu-slide a.deleteCar').hasClass('deleteCar')) $('.menu-slide').append('<li><a class="deleteCar">Delete Carousel</a></li>');
   initCarousel();
   $('.carouselTools').show();
}

function get_these(url) {
    if($('body').hasClass('loaded')) $('body').attr('class','loading');
    selected=new Array();
    $('.pagination, .total').hide();
    $('.pagination a').attr('style','');
    setTimeout(function(){
     $('#itemsjs').remove();
     var script = document.createElement('script');
     script.src = base + url;
     script.id = 'itemsjs';
     $('head').append(script);
    },300);
}

function get_items() {
    document.activeElement.blur();
    $('input').blur();
    if($('body').hasClass('loaded')) $('body').attr('class','loading');
    $('#itemsjs').remove();
    var limit=Number($('#item-limit a.active:eq(0)').text());
    var types = '';
    $('#item-type a.active').each(function(){
     types +='type[]='+$(this).text()+'&';
    });
    var browse = '';
    if($('#browse ul a.active').attr('data-xid')) browse = 'browse[]='+escape($('#browse a.active').attr('data-xid'));
    $('#browse a.active').parents('ul ul[id]').each(function(){
      browse = 'browse[]='+escape($(this).attr('id').replace('x','')) + '&' + browse;
    });
    if(browse) browse='&'+browse;
    var q='&q='+escape($('#search').val());

    var script = document.createElement('script');
    script.type = 'text/javascript';
    var get_items_req_new = $('#item-sort .active:eq(0)').attr('rel') + (($('#item-sort .active:eq(0)').hasClass('desc')) ? '-desc' : '') + browse + q + types.replace(/\&+$/, '') + limit;
    if(get_items_req !== get_items_req_new) {
     $('#filtersAssets').html('');
     items_page=1;
     selected=new Array();
    }
    get_items_req = get_items_req_new;
    script.src = base+'GetItems_JSONP.aspx?page='+items_page+ '&sort='+ $('#item-sort .active:eq(0)').attr('rel') + (($('#item-sort .active:eq(0)').hasClass('desc')) ? '-desc' : '') + browse + q +'&'+types.replace(/\&+$/, '')+'&limit='+ limit + '&x='+x();
    script.id = 'itemsjs';
    $('head').append(script);
}

function get_filters() {
  $('#filtersAssets').html('');
  $('.filters.opener').addClass('loading');
  if($('body').hasClass('loaded')) $('body').attr('class','loading');
  $('#filterjs').remove();
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = base + 'GetFilterListItems_Asset_JSONP.aspx';
  script.id = 'filterjs';
  $('head').append(script);
}

function jsonp_browse_list_items_asset(data) {
  if (data.rows.value !== null && typeof data.rows.value === 'object' ) {
    $('#filtersAssets').html('');
    $('.pagination').hide();
    if(data.rows.value.length>0){
     var content = new Array();
     for (var i = 0; i < data.rows.value.length; i++) { 
      content.push('<li>');
      content.push('<div class="assets-accord-holder">');
      content.push('<a class="opener '+data.rows.value[i].type+' accord">'+ data.rows.value[i].name +'</a>');

      content.push('</div>');
      content.push('<div class="slide">');
      content.push('<ul>');
      for (var ii = 0; ii < data.rows.value[i].children.length; ii++) { 
       content.push('<li>');
       if(data.rows.value[i].children[ii].Url_Remove) content.push('<a class="close" rel="'+ data.rows.value[i].children[ii].Url_Remove +'" title="'+ data.rows.value[i].children[ii].Name +'"><em>'+ data.rows.value[i].children[ii].Name +'</em></a>');
       else if(data.rows.value[i].children[ii].Url) content.push('<a rel="'+ data.rows.value[i].children[ii].Url +'" title="'+ data.rows.value[i].children[ii].Name +'"><em>'+ data.rows.value[i].children[ii].Name +'</em></a>');
       content.push('<span class="current-number">'+ data.rows.value[i].children[ii].count +'</span>');
       content.push('</li>');
      }
      content.push('</ul>');
      content.push('</div>');
      content.push('</li>');
     }
     $('#filtersAssets').html(content.join(''));
     initAccordion();
     $('#filtersAssets a[rel]').unbind();
     $('#filtersAssets a[rel]').click(function(){
       get_these($(this).attr('rel'));
     });
    }
  }
  $('.filters.opener').removeClass('loading');
  $('body').attr('class','loaded');
}

function jsonp_items(data) {
  $('#item-list').html('');
  $('#item-table tbody').remove();
  if(data && data.rows) {
    var content = new Array();
    content.push('<ul class="img-list">');
    var content2 = new Array();
    content2.push('<tbody>');
    for (var i = 0; i < data.rows.length; i++) { 
      content.push('<li data-type="'+ data.rows[i].value.type +'" data-id="'+ data.rows[i].value._id +'">');
      content.push('<div class="img-holder">');
      content.push('<a><div class="lz-loading"></div><img class="lazy" src="'+  data.base + '&type='+data.rows[i].value.type+'&id='+data.rows[i].value._id+'&size=2&width=340&height=191&qfactor=12' +'" title="'+data.rows[i].value.name+'" alt="'+data.rows[i].value.name+'"></a>');      
      content.push('<div class="rollover-block">');
      if(data.rows[i].value.VideoPLayerObject) content.push('<iframe longdesc="'+ data.rows[i].value.VideoPLayerObject +'&Width=340&Height=191" width="340" height="191" frameborder="0" scrolling="no" seamless="seamless" class="quickVideoFrame" />');
      else content.push('<a><img class="lazy" src="'+data.base +'&type='+data.rows[i].value.type+'&id='+data.rows[i].value._id+'&size=2&width=340&height=191&qfactor=12'+'" title="'+data.rows[i].value.name+'" alt="'+data.rows[i].value.name+'"></a>');

      content.push('<div class="description-block">');
      content.push('<dl>');
      content.push('<dt>NAME</dt>');
      content.push('<dd>'+ data.rows[i].value.name  +'</dd>');
      content.push('<dt>PROJECT</dt>');
      content.push('<dd><a>'+ data.rows[i].value.project +'</a></dd>');
      content.push('<dt>SIZE / TYPE</dt>');
      content.push('<dd>'+ bytesToSize(data.rows[i].value.size) +' /  '+ data.rows[i].value.type +'</dd>');
      content.push('<dt>DATE</dt>');
      content.push('<dd>'+ data.rows[i].value.timestamp.split(' ')[0] +'</dd>');
      content.push('</dl>');
      content.push('</div>');
      content.push('</div>');
      content.push('</div>');
      content.push('<div class="ico-row">');
      content.push('<a class="download single"></a>');
      content.push('<a class="add"></a>');
      content.push('<a href="#details" data-type="'+ data.rows[i].value.type + '" rel="'+ data.rows[i].value._id +'" class="information lightbox"></a>');
      content.push('<em class="rating rating'+ data.rows[i].value.rank +'"></em>');
      content.push('<div class="downList d'+data.rows[i].value._id+'">');
      content.push('</div>');
      content.push('</div>');
      content.push('<span class="name">'+ data.rows[i].value.name +'</span>');
      content.push('</li>');
      content2.push('<tr data-type="'+ data.rows[i].value.type +'" data-id="'+ data.rows[i].value._id +'">');
      content2.push('<td class="item1">');
      content2.push('<a class="checkbox"></a>');
      content2.push('<div class="img-area">');
      content2.push('<img src="'+  data.base +'&type='+data.rows[i].value.type+'&id='+data.rows[i].value._id+'&size=2&width=340&height=191&qfactor=12' +'"  alt="'+data.rows[i].value.name+'" class="small-img">');
      content2.push('<div class="rollover-block">');


      if(data.rows[i].value.VideoPLayerObject) content2.push('<iframe longdesc="'+ data.rows[i].value.VideoPLayerObject +'&Width=340&Height=191" width="340" height="191" frameborder="0" scrolling="no" seamless="seamless" class="quickVideoFrame" />');
      else content2.push('<img src="'+  data.base +'&type='+data.rows[i].value.type+'&id='+data.rows[i].value._id+'&size=2&width=340&height=191&qfactor=12' +'"  alt="'+data.rows[i].value.name+'">');
      
      content2.push('<div class="description-block">');
      content2.push('<dl>');
      content2.push('<dt>NAME</dt>');
      content2.push('<dd>'+ data.rows[i].value.name  +'</dd>');
      content2.push('<dt>PROJECT</dt>');
      content2.push('<dd><a>'+ data.rows[i].value.project +'</a></dd>');
      content2.push('<dt>SIZE / TYPE</dt>');
      content2.push('<dd>'+ bytesToSize(data.rows[i].value.size) +' /  '+ data.rows[i].value.type +'</dd>');
      content2.push('<dt>DATE</dt>');
      content2.push('<dd>'+ data.rows[i].value.timestamp.split(' ')[0] +'</dd>');
      content2.push('</dl>');
      content2.push('</div>');
      content2.push('</div>');
      content2.push('</div>');
      content2.push('</td>');
      content2.push('<td class="item2"><span>'+ data.rows[i].value.name  +'</span></td>');
      content2.push('<td class="item3">'+ bytesToSize(data.rows[i].value.size) +'</td>');
      content2.push('<td class="item4">'+ data.rows[i].value.type +'</td>');
      content2.push('<td class="item8">'+ data.rows[i].value.timestamp.split(' ')[0] +'</td>');
      content2.push('<td class="item5">Internal User</td>');
      content2.push('<td class="item6">');
      content2.push('<div class="rating-holder">');
      content2.push('<em class="rating rating'+ data.rows[i].value.rank +'"></em>');
      content2.push('<div class="tooltip-block">Rating</div>');
      content2.push('</div>');
      content2.push('</td>');
      content2.push('<td class="item7">');
      content2.push('<ul class="ico-area">');
      content2.push('<li>');
      content2.push('<a class="download single"></a>');
      content2.push('</li>');
      content2.push('<li>');
      content2.push('<a class="add"></a>');
      content2.push('<div class="tooltip-block">Add to Carousel</div>');
      content2.push('</li>');
      content2.push('<li>');
      content2.push('<a href="#details" data-type="'+data.rows[i].value.type+'" rel="'+ data.rows[i].value._id +'" class="information lightbox"></a>');
      content2.push('<div class="tooltip-block">More Information</div>');
      content2.push('</li>');
      content2.push('</ul>');
      content2.push('</td>');
      content2.push('</tr>');
    }
    content.push('</ul>');
    content2.push('</tbody>');

    items_count=Number(data.total_count);
    $('.total strong').html(items_count);
    if(items_count==1) $('.total span').html('item found');
    else $('.total span').html('items found');
    var totalpages = Math.ceil(items_count / Number($('#item-limit a.active').text()));
    $('.currentpage').val(items_page);
    $('.totalpages').html(totalpages);

    if(totalpages<1 || items_page > totalpages) $('.pagination').hide();
    else $('.pagination').show();

    if(items_page>1) $('.pagination .prev').css('visibility','visible');
    else $('.pagination .prev').css('visibility','hidden');

    if(totalpages>1 && items_page<totalpages) $('.pagination .next').css('visibility','visible');
    else $('.pagination .next').css('visibility','hidden');

    $('#item-list').html(content.join(''));
    $('#item-table').append(content2.join(''));
    callLzLoad();
 }
 else {
        $('#list-layout').html('');
 }
 initDownload();
 initGetDetails();
 initLayoutFix();
 initTooltip();
 // callLzLoad();
 if(data && data.rows) {
  for (var i = 0; i < data.rows.length; i++) { 
   if(selected.indexOf(data.rows[i].value._id)>-1) {
     $('#item-list li[data-id="'+data.rows[i].value._id+'"] >div:eq(0)').addClass('active');
     $('#item-table tr[data-id="'+data.rows[i].value._id+'"]').addClass('active');
   }
  }
 }
 if($('#filtersAssets a[rel]').is('a')) {
  get_filters();
 }
 else {
  if(loadI>2) $('body').attr('class','loaded');
  if(i) $('.filters.opener').css('visibility','visible');
  loadI++; 
 }
 
}  

function get_downloads(id) {
    $('#browsedownloads').remove();
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = base+'getItemDownloads_JSONP.aspx?id='+id;
    script.id = '#browsedownloads';
    $('head').append(script);
}

function jsonp_item_downloads(data){
  if(data.value) {
    if(data.value.DownloadTypes) {
     var x = '';
     $.each(data.value.DownloadTypes, function (key,obj) { 
       if(data.value.DownloadTypes[key].URL) x +='<li><a href="'+data.value.DownloadTypes[key].URL+'">'+data.value.DownloadTypes[key].Type+'</a></li>';
       else if(data.value.DownloadTypes[key].Type) x +='<li><a class="nolink">'+data.value.DownloadTypes[key].Type+'</a></li>';
     }); 
     if(x) x='<ul class="slide">'+x+'</ul>';
     if($('#tab4').hasClass('js-tab-hidden')){
       $('.downList.d'+ data.value._id).html(x);
       $('.downList.d'+ data.value._id).siblings('.download').trigger('mouseleave');
       initOpenCloseDownloads();
       $('.downList.d'+ data.value._id).siblings('.download').trigger('mouseenter');
     }
     else {

      $('#tDown').remove();
      $('body').append('<div id="tDown"></div>');
      $('#tDown').html(x);

     }
   }
  }
}

function browse_list_items() {
    $('#browsejs').remove();
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = base+'GetBrowseListItems_JSONP.aspx';
    script.id = 'browsejs';
    $('head').append(script);
}

function jsonp_browse_list_items(data) { 
   if(data.rows && data.rows.value && data.rows.value.length>0) {
   var content = new Array(); 
   if(data.CATEGORY_ID)  {
    content.push('<div class="accordion-holder">');
    content.push('<ul class="accordion" id="x'+data.CATEGORY_ID+'">');
    for (var i = 0; i < data.rows.value.length; i++) { 
        content.push('<li><a rel="'+ data.rows.value[i].url +'" data-xid="'+ data.rows.value[i].url.split("=").pop() +'" class="'+data.rows.value[i].type+'" title="'+data.rows.value[i].name+'"><em>'+data.rows.value[i].name+'</em></a></li>');
    }
    content.push('</ul>');
    content.push('</div>');
    $('#browse a[rel*="='+ data.CATEGORY_ID +'"]').after(content.join(''));
    $('#x'+data.CATEGORY_ID).slideDown(200);
   }
   else if(data.ProjectId)  {
    content.push('<div class="accordion-holder">');
    content.push('<ul class="accordion" id="x'+data.ProjectId+'">');
    for (var i = 0; i < data.rows.value.length; i++) { 
     content.push('<li><a rel="'+ data.rows.value[i].url +'" data-xid="'+ data.rows.value[i].url.split("=").pop() +'" class="'+data.rows.value[i].type+'" title="'+data.rows.value[i].name+'"><em>'+data.rows.value[i].name+'</em></a></li>');
    }
    content.push('</ul>');
    content.push('</div>');
    $('#browse a[rel*="='+ data.ProjectId +'"]').after(content.join(''));
    $('#x'+data.ProjectId).slideDown(200);
   }
   else if(data.Folder_CATEGORY_ID)  {
    content.push('<div class="accordion-holder">');
    content.push('<ul class="accordion" id="x'+data.Folder_CATEGORY_ID+'">');
    for (var i = 0; i < data.rows.value.length; i++) { 
        content.push('<li><a rel="'+ data.rows.value[i].url +'" data-xid="'+ data.rows.value[i].url.split("=").pop() +'" class="'+ data.rows.value[i].type +'" title="'+data.rows.value[i].name+'"><em>'+data.rows.value[i].name+'</em></a></li>');
    }
    content.push('</ul>');
    content.push('</div>');
    $('#browse a[rel*="='+ data.Folder_CATEGORY_ID +'"]').after(content.join(''));
    $('#x'+data.Folder_CATEGORY_ID).slideDown(200);
   }

   else {
    for (var i = 0; i < data.rows.value.length; i++) { 
        if(data.rows.value[i].type=='project') content.push('<a rel="'+ data.rows.value[i].url +'" data-xid="'+ data.rows.value[i].url.split("=").pop() +'" class="'+ data.rows.value[i].type +'">'+data.rows.value[i].name+'</a>');  
        else content.push('<a rel="'+ data.rows.value[i].url +'" data-xid="'+ data.rows.value[i].url.split("=").pop() +'" class="top-level-box close">'+data.rows.value[i].name+'</a>');  
    }
    $('#browse').html(content.join(''));
   }
   $('#browse a').unbind();
   $('#browse a[rel]').click(function() {
    $('#search').val('');
    if($(this).hasClass('close') && $('a.active span.ldr').length !=1 ) {
      $(this).append('<span class="ldr"></span>');
      $('span.ldr').css('display','block');
     } else {
      $('span.ldr').css('display','none');
      $('span.ldr').remove();
      $(this).addClass('close');
    }
    if($(this).hasClass('active')) {
     $(this).removeClass('active');
     $(this).parent().find('.accordion-holder').stop().slideUp(200);
     return false;
   }
   else if($(this).hasClass('upstream') && $(this).hasClass('top-level-box')) {
    $('#browse a.active').removeClass('active');
    $('#browse a.upstream').removeClass('upstream');
    $('#browse .accordion-holder').slideUp(200);
    return false;
   }
   else if($(this).hasClass('upstream')) {
    $('#browse a.active').removeClass('active');
    $('#browse a.upstream, #browse').removeClass('upstream');
    if($(this).parent().is('li')) $(this).addClass('active');
     $('#browse a.active').parents('ul[id]').each(function(){
       $('#browse a[data-xid="'+$(this).attr('id').replace('x','')+'"]').addClass('upstream'); 
     });
    $(this).parent().find('.accordion-holder .accordion-holder ').slideUp(200);
    get_items();
    return;
   }
   if($(this).hasClass('active')) {
    $('#browse a.active').removeClass('active');
    $('#browse a.upstream').removeClass('upstream');
    $('#browse .accordion-holder').slideUp(200);
    return false;
   }
   else {
     $('#browse a.active').removeClass('active');
     $('#browse a.upstream').removeClass('upstream');
     if($(this).parent().is('li')) $(this).addClass('active');
     $('#browse .accordion-holder.active').removeClass('active');
     $(this).parents('.accordion-holder').addClass('active');
     $(this).addClass('active');
     $('#browse a.active').parents('ul[id]').each(function(){
       $('#browse a[data-xid="'+$(this).attr('id').replace('x','')+'"]').addClass('upstream'); 
     });
     $('#browse .accordion-holder').not('.active').slideUp(200);
     if(!$(this).hasClass('top-level-box')) get_items();
    }

    $('#browsejs').remove();
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = base + $(this).attr('rel');
    script.id = 'browsejs';
    $('head').append(script);
   });
  }
  else {
  if(loadI<6) $('.browse.opener').hide();
 }
 loadI++; 
} 

function carousel_add_asset(asset) {
    var car = $('#carname').attr('data-car');
    $('#carjs').remove();
    var script = document.createElement('script');
    script.src = base+'AddAssetToCarousel_JSONP.aspx?asset_id[]='+ asset +'&carousel_id='+ car +'&x='+x();
    script.id = 'carjs';
    $('head').append(script);
}

function jsonp_AddAssetToCarousel(data) {
    if(data.status=="true") get_car(data.carousel_id);
    else alert(data.message );
}

function carousel_remove_asset(asset) {
    var car = $('#carname').attr('data-car');
    $('#carjs').remove();
    var script = document.createElement('script');
    script.src = base+'RemoveAssetFromCarousel_JSONP.aspx?asset_id='+ asset +'&carousel_id='+ car +'&x='+x();
    script.id = 'carjs';
    $('head').append(script);
}

function get_details(type,id) {
    $('#details-nav').html('&nbsp;');
    if($('body').hasClass('loaded')) $('body').attr('class','loadingDetails');
    $('#itemdetailsjs').remove();
    var browse = '';
    var script = document.createElement('script');
    script.src = base+'getItemDetails_JSONP.aspx?type='+type+'&id='+id+'&x='+x();
    script.id = 'itemdetailsjs';
    $('head').append(script);
}

function jsonp_RemoveAssetFromCarousel(data){
    if(data.status=="true") get_car(data.carousel_id);
    else alert(data.message);
}

function jsonp_item_details(data) { 
    $('#detailsContent').html('');    
    var content = new Array();
    content.push('<div class="img-block">');
    content.push('<div>');
    content.push('<div class="mask">');
    content.push('<div class="slideset">');
    content.push('<div class="slide">');
    if(data.value.VideoPlayerObject) content.push('<iframe src="'+ data.value.VideoPlayerObject +'" frameborder="0" scrolling="no" seamless="seamless" width="648" height="360" id="vframe" />');
    else if(data.value.base) content.push('<img src="'+ data.value.base +'&type='+data.value.type+'&id='+data.value._id+'&size=2&width=1648&height=360&qfactor=12" alt=""/>');
    content.push('</div>');
    content.push('</div>');
    content.push('</div>');
    content.push('</div>');
    content.push('<ul class="btn-list">');
     if(data.value.DownloadTypes) {
     content.push('<li>');
     content.push('<a class="download opener">DOWNLOAD</a>');
     content.push('<ul class="slide">');
     for (var i = 0; i < data.value.DownloadTypes.length; i++) { 
       content.push('<li><a href="'+ data.value.DownloadTypes[i].URL +'">'+ data.value.DownloadTypes[i].Type +'</a></li>');
     }
     content.push('</ul>');
     content.push('</li>');
    }
    //content.push('<li><a href="#" class="share">SHARE / EMAIL</a></li>');
    //content.push('<li><a href="#" class="drag">DRAG & DROP</a></li>');
    content.push('<li data-id="'+data.value._id+'"><a class="add">ADD CAROUSEL</a></li>');
    content.push('</ul>');
    content.push('</div>');
    content.push('<div class="slideshow description-area">');
    content.push('<div class="slideshow-slideset">');
    content.push('<div class="slideshow-slide">');
    content.push('<div class="description-box">');
    content.push('<div class="holder">');
    content.push('<a class="opener">General Information</a>');

    content.push('<ul class="slide">');
    content.push('<li><a>General Information</a></li>');
    content.push('<li><a>Advanced Information</a></li>');
    content.push('</ul>');
    content.push('</div>');
    content.push('</div>');
    content.push('<dl>');
    if(data.value.name) content.push('<dt>NAME</dt><dd>'+ data.value.name +'</dd>');
    if(data.value.description) content.push('<dt>DESCRIPTION</dt><dd>'+ data.value.description + '</dd>');
    if(data.value.project) content.push('<dt>COLLECTION</dt><dd>'+ data.value.project +'</dd>');
    if(data.value.associated) content.push('<dt>ASSOCIATED</dt><dd>'+ data.value.associated +'</dd>');
    if(data.value.version) content.push('<dt>VERSION</dt><dd>'+ data.value.version +'</dd>');
    content.push('</dl>');

    content.push('<dl>');
    if(data.value._id) content.push('<dt>ID</dt><dd>'+ data.value._id +'</dd>');
    if(data.value.type) content.push('<dt>TYPE</dt><dd>'+ data.value.type +'</dd>');
    if(data.value.timestamp) content.push('<dt>DATE</dt><dd>'+ data.value.timestamp +'</a></dd>');
    if((typeof(data.value.rating) != "undefined")) content.push('<dt>RATING</dt><dd>'+ data.value.rating +'</a></dd>');
    if(data.value.size) content.push('<dt>SIZE</dt><dd>'+ bytesToSize(data.value.size) +'</a></dd>');
    if(data.value.directories) content.push('<dt>DIRECTORY</dt><dd>'+ data.value.directories.replace(/\|/g,' / ').replace(/ \/ $/g,'') + '</dd>');
    content.push('</dl>');

    content.push('</div>');
    content.push('</div>');
    content.push('</div>');
    $('#detailsContent').html(content.join(''));
    $('#detailsContent .slideshow .slideshow-slide dl:eq(0)').show();

    initDetailsOpenClose();
    $('.lightbox .slide-holder .description-area').css('height', $(window).height() - ($('#footer').outerHeight() + $('#header').outerHeight() + $('.search-block').outerHeight() + 197 ) + 'px');
    setTimeout(function(){if($('body').attr('class')) $('body').attr('class','loaded');},30);
    setTimeout(function(){get_item_place(data.value.type,data.value._id);},550);
}

function get_item_place(type,id) {
    $('#itemplacejs').remove();
    var types = '';
    $('#item-type a.active').each(function(){
     types +='type[]='+$(this).text()+'&';
    });
    var browse = '';
    $('.slide-bars .browse-list ul li a.active').parents('.slide-bars .browse-list ul li').each(function(){
      browse = 'browse[]='+escape($(this).find('a:eq(0)').attr('rel')) +'&'+ browse;
    });
    if(browse) browse='&'+browse;
    var q='&q='+escape( $('#search').val());
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = base + 'item_place.aspx?type='+type+'&id='+id+'&'+ browse + q +'&'+types.replace(/\&+$/, '')+'&limit=9999&x='+x();
    script.id = 'itemplacejs';
    $('head').append(script);
}

function jsonp_item_place(data) {
  $('#details-nav').html('&nbsp;');
  if(data.total > 0 && data.place > 0) {
    var content = new Array();
    if(data.prev) content.push('<a data-id="'+ data.prev._id +'" data-type="'+ data.prev.type +'"  class="btn-prev">Previous</a>');
    content.push('<span class="details-current-num">'+ data.place +'</span>');
    content.push(' of ');
    content.push('<span class="details-total-num">'+ data.total +'</span>');
    if(data.next) content.push('<a data-id="'+ data.next._id +'" data-type="'+ data.next.type +'" class="btn-next">Next</a>');
    $('#details-nav').html(content.join(''));
    imageWidthCal();
  }
}

function getPP() {
 if(selected.length > 0) {
   window.open(base + 'getPPT.aspx?Heading=HelloWorldTitle&FileName='+ (($('#ppFile').prop('checked')) ? 'true' : 'false') +'&ProjectName='+ (($('#ppProject').prop('checked')) ? 'true' : 'false') +'&Notes='+ (($('#ppNotes').prop('checked')) ? 'true' : 'false') +'&id=' +selected.join(','));
   //window.open(base + 'getPPT.aspx?Heading=HelloWorldTitle&FileName='+ (($('#ppFile').prop('checked')) ? 'true' : 'false') +'&ProjectName='+ (($('#ppProject').prop('checked')) ? 'true' : 'false') +'&Notes='+ (($('#ppNotes').prop('checked')) ? 'true' : 'false') +'&id=' +selected.join(','),'pp','height=1,width=1,scrollbars=no,toolbar=no,location=no,status=no,menubar=no,resizable=no,dependent=no');
  }
  else alert('You must first select one or more items.');
}

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Bytes';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

function x() {
  return String(new Date().getTime())+String(Math.floor(Math.random()*9999))
}

function testReady() {
  if(loadI >=3) init();
  else setTimeout(function(){testReady()},250);
}

function imageWidthCal() {
  var nextImgWidth = $('.slide').find('img').width();
  $('.nav-row').css('width', nextImgWidth + 'px');
}

function calImageWidthW() {
  $('.slide img').load(function() {
    var navRowWidth = $(this).width();
    $('.nav-row').css('width', navRowWidth + 'px');
  });
}

function callLzLoad() {
  if(('img.lazy').length > 0) {
    $('img.lazy').load(function() {
      $('.lz-loading').css('display','none');
      $('img.lazy').css('opacity','1');
    });
  }
}


